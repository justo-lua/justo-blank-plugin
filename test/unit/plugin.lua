--imports
local assert = require("justo.assert")
local justo = require("justo")
local suite, test = justo.suite, justo.test

--suite
return suite("xxx", function()
  test("exported tasks", function()
    local plugin = require("xxx")

    assert(plugin):isTable()

    for _, name in ipairs({}) do
      local task = plugin[name]

      assert(task):isTable()
      assert(task.type):eq("simple")
    end
  end)
end)
