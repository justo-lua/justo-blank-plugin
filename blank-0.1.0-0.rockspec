package = "blank"
version = "0.1.0-0"

description = {
  summary = "XXX.",
  detailed = [[
    Plugin for XXX.
  ]],
  license = "",
  homepage = "http://XXX",
  maintainer = "Developer name <email>"
}

source = {
  url = ""
}

dependencies = {
  "lua >= 5.2, <= 5.3",
  "justo >= 1.0.alpha1"
}

-- devDependencies = {
--   "justo-cli >= 1.0.alpha1",
--   "justo-assert >= 1.0.alpha1",
--   "justo-plugin-cli >= 1.0.alpha1"
-- }

build = {
  type = "builtin",

  modules = {

  }
}
