--imports
local catalog = require("justo").catalog
local cli = require("justo.plugin.cli")

--catalog
catalog:macro("lint", {
  {title = "Check source code", task = cli, params = {cmd = "luacheck --codes ."}},
  {title = "Check rockspec", task = cli, params = {cmd = "luarocks lint *.rockspec"}}
}):desc("Check source code.")

catalog:call("make", cli, {
  cmd = "luarocks make --local"
}):desc("Make the rock.")

catalog:macro("build", {
  {title = "Check source code", task = "lint"},
  {title = "Make and install", task = "make"}
}):desc("Lint, make and install.")

catalog:workflow("test", function(params)
  require(params[1])()
end):desc("Unit testing."):iter(

)

catalog:macro("default", {
  {title = "Check source code", task = "lint"},
  {title = "Make and install", task = "make"},
  {title = "Test", task = "test"}
}):desc("Lint, make, install and test.")
